package objs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//https://www.seleniumeasy.com/selenium-tutorials/how-to-work-with-iframes-in-selenium-webdriver

public class iframePage {
	
	WebDriver browser;
	
	
	 
	
	public iframePage(WebDriver browser) {
		
		this.browser = browser;
	}
	
	public iframePage framePage() {
		
		browser.get("https://the-internet.herokuapp.com/iframe");
		
		return this;
	}
	
	
	
	public void typeSomeInfrmae(String text) {
	    
		try {
		
	    // find frame by id
		//WebElement f =   browser.findElement(By.id("mce_0_ifr"));
		//browser.switchTo().frame(f);
		
		// find frame by index
		browser.switchTo().frame(0);
		System.out.println("switch to frame(0) and click on File");
		WebElement textbox = browser.findElement(By.id("tinymce"));
		textbox.clear();
		textbox.sendKeys(text);
		Thread.sleep(3000);
		
		//switch to parent 
		browser.switchTo().defaultContent();
		System.out.println("switch to parent frame and click on File");
		WebElement file = browser.findElement(By.xpath("//span[text()=\"File\"]"));
		file.click();
		Thread.sleep(3000);
		file.click();
		
		WebElement b = browser.findElement(By.xpath("//button[@title=\"Formats\"]"));
		b.click();
		Thread.sleep(3000);
		b.click();
		Thread.sleep(3000);
		// go to frame again
		browser.switchTo().frame(browser.findElement(By.id("mce_0_ifr")));
		textbox.clear();
		Thread.sleep(3000);
		textbox.sendKeys(text);
		
		
		}catch (Exception error) {
			error.printStackTrace();
		}
		
	}

}
