package objs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
	
	WebDriver browser;
	
	public LoginPage(WebDriver browser) {
		
		this.browser = browser;
	}
	
	public LoginPage loginUrl() {
		
		browser.get("https://the-internet.herokuapp.com/login");
		
		return this;
	}
	
	public LoginPage typeUserName(String user) {
		
		browser.findElement(By.xpath("//input[@name='username']")).sendKeys(user);
		
		return this;
	}
	
	public LoginPage typePassword(String pw) {
		
		browser.findElement(By.xpath("//input[@name='password']")).sendKeys(pw);
		
		return this;
	}
	
	
	public LoginPage loginbutton () {
		
		
		browser.findElement(By.xpath("//button[@type='submit']")).click();
		
		return this;
	}
	
	public String HomePage () {
		
		return browser.findElement(By.xpath("//h2")).getText();
		
		//return browser.getTitle();
	}

}
