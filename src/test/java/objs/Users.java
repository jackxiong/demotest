package objs;

import com.opencsv.bean.CsvBindByPosition;

public class Users {
	
	@CsvBindByPosition(position = 0)
	private String country;
	@CsvBindByPosition(position = 1)
	private String email;
	@CsvBindByPosition(position = 2)
	private String AfA;
	
	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAfA() {
		return AfA;
	}

	public void setAfA(String afA) {
		AfA = afA;
	}

	
	
	public  Users() {
		
	}

}
