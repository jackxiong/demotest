package objs;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GooglePage {
	
	WebDriver browser;
	
	
	
	public GooglePage(WebDriver browser) {
		
		this.browser=browser;
	}
	
	public void url(String url) {
		browser.get(url);
	}
	
	public GooglePage type(String text) {
		
		browser.findElement(By.xpath("//input[@name='q']")).sendKeys(text);
		return this;
	}
	
	public GooglePage search() {
		
		
		 browser.findElement(By.name("q")).sendKeys(Keys.ENTER);
		 return this;
	}
	
	public void clickResult() {
		
		 browser.findElement(By.xpath("//h3")).click();
		
	}
	
	public String title () {
		
		return browser.getTitle();
	}

}
