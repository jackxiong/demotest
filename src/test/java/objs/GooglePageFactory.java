package objs;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GooglePageFactory {
	
	  @FindBy(xpath="//input[@name='q']")
	  WebElement type;
	  
	  @FindBy(name="q")
	  WebElement search;
	  
	  @FindBy(xpath="//h3")
	   WebElement result;
	  
	  
	  
	  
	  public GooglePageFactory(WebDriver driver) {
		  PageFactory.initElements(driver, this); 
	  }
	  
	 
	  
	  public GooglePageFactory typeString(String text) {
		  type.sendKeys(text);
		  return this;
	  }
	  
	  public GooglePageFactory clickSerch() {
		  search.sendKeys(Keys.ENTER);
		  return this;
	  }
	  
	  public GooglePageFactory clickResult() {
		   result.click();
		  return this;
	  }

}
