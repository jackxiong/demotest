package com.test.api;

import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.response.Response;
import io.restassured.path.json.JsonPath;
import static io.restassured.RestAssured.given;

import java.util.List;

public class TestApi {

	
	@Test
	public void testFindRooms() {
		
		String url = "https://automationintesting.online/room/";
		Response res = given().get(url);
		System.out.println(res.getBody().prettyPeek());
		JsonPath jp = new JsonPath(res.asString());
		
		
		List<Object> l = res.jsonPath().getList("rooms");
		System.out.println("l size=>"+l.size());
		Assert.assertTrue(l.size()!=0);
		
		System.out.println("rooms size=>"+jp.getInt("rooms.size()"));
		System.out.println(jp.getInt("rooms[0].roomid"));
		System.out.println(jp.getInt("rooms[0].roomNumber"));
		
		String str = System.getProperty("devicename");
		System.out.println("str=>"+str);
		
		
		int i=0;
		String r = "rooms["+i+"].type";
		String roomtype = jp.get(r);
		System.out.println("type = >"+roomtype);
		
		//Assert.assertEquals(200,res.statusCode());
		
		
	}
}
