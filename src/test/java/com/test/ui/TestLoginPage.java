package com.test.ui;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import base.Hook;
import base.TestBase;
import objs.LoginPage;
import objs.iframePage;


public class TestLoginPage extends TestBase{

	LoginPage login ;
	iframePage ifp;
	@Test(dataProvider = "test1", enabled=false)
	public void testLogin(String user,String pw) {
		
		
		login = new LoginPage(driver);
		login.loginUrl();
		login.typeUserName(user)
		.typePassword(pw)
		.loginbutton();
		
		Assert.assertEquals(login.HomePage(),"Secure Area");
	
	}
	
	@DataProvider(name = "test1")
	public Object[][] createData1() {
	 return new Object[][] {
	   { "tomsmith","SuperSecretPassword!"},
	   //{ "test","SuperSecretPassword!"},
	 };
	}
	
	@Test
	public void testFramePage() {
		ifp = new iframePage(driver);
		ifp.framePage().typeSomeInfrmae("this is test iframe page");
	}
}
