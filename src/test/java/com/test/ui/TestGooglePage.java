package  com.test.ui;


import java.util.Properties;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import base.TestBase;
import objs.GooglePage;

//https://bitbucket.org/jackxiong
public class TestGooglePage extends TestBase{
	
	GooglePage gp ;
	Properties prop;

	@Test(dataProvider = "createDataGoolge")
	public void test_google_page(String text) {
		

		gp=new GooglePage(driver);
		gp.url((String)getProp().get("url"));
		System.out.println("this is test google page for "+text);
		
		
		gp.type(text).search();
		System.out.println("=>"+gp.title());
		Assert.assertEquals(gp.title(),text+" - Google Search");
		gp.clickResult();
		
		//Hook.takeSnapShot(driver,text);
		//String d = System.getProperty("devicename");
		//System.out.println("dvicename "+d);
	}
	
	@DataProvider
	public Object[][] createDataGoolge() {
	 return new Object[][] {
	   { "jeff bezos"},
	   { "australia covid cases"},
	   {"Chartered Accountants Australia and New Zealand"},
	 };
	}
	 
    

}
