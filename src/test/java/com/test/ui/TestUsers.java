package com.test.ui;
//https://springhow.com/opencsv/
//http://opencsv.sourceforge.net/apidocs/index.html
//https://www.callicoder.com/java-read-write-csv-file-opencsv/
//https://www.baeldung.com/opencsv
//https://www.geeksforgeeks.org/writing-a-csv-file-in-java-using-opencsv/
//https://howtodoinjava.com/log4j/how-to-configure-log4j-using-properties-file/
//https://www.cnblogs.com/chinajava/p/5721563.html

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;


import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;

import objs.Users;
import sources.urlResources;

public class TestUsers {
	
	 
    List<Users> l;
    static Logger log = Logger.getLogger(TestUsers.class);
    urlResources source;
    
    //static Logger logger = Logger.getLogger(Log4jPropertiesConfigurationExample.class);
   
	@Test
	public void test_users() {
		
	PropertyConfigurator.configure("log4j.properties");
	//BasicConfigurator.configure();
	source =  urlResources.valueOf("sit_salesforce");
	
	System.out.println(source.qa_user.ordinal());
		
		Users user = new Users();
		user.setCountry("AU");
		user.setEmail("jackxiong@82@gmail.com");
		user.setAfA("Afa12345");
		
		Users user1 = new Users();
		user1.setAfA("Nz0091");
		user1.setEmail("jack@gmail.com");
		user1.setCountry("NZ");
	
		l = new ArrayList<Users>();
		
		for (int i=0;i<1;i++) {
			l.add(user);
			//l.add(user1);
		}
		
		
		String path ="./csv/users.csv";
		//System.getProperty("user.dir")+"/src/test/java/log/users.csv";
		try {
			
			writeBeantoCsv(l,path);
		}catch(Exception error) {
			error.printStackTrace();
		}
		
		csvfile("./csv/users.csv");
		
		//System.out.println("writetoCsv file "+System.getProperty("user.dir")+"/src/test/java/log/users.csv");
		log.info("Hello this is an info message "+source.getPath());
		log.debug("Log4j appender configuration is successful ....!!");
	}
	
	
	
	public void writeBeantoCsv(List<Users> l,String path) {
		
		try {
			List<Users> employees = l;
			Writer writer = new FileWriter(path,true);
			StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer)
					.withSeparator(CSVWriter.DEFAULT_SEPARATOR)
		            .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
		            .withEscapechar(CSVWriter.DEFAULT_ESCAPE_CHARACTER)
		            .withLineEnd(CSVWriter.DEFAULT_LINE_END)
		            .withOrderedResults(false)
					.build();
			
			beanToCsv.write(employees);
			writer.close();
		}catch (Exception error) {
			error.printStackTrace();
		}
		
	}
	
	
	//mFileWriter = new FileWriter(file_path, true);
	//mCsvWriter = new CSVWriter(mFileWriter);
	public List<Users> csvfile(String cf) {
		
		List<Users> lu = new ArrayList();
		
		String csvfile = cf;
		CSVReader reader = null;  
		try  
		{  
		
		reader = new CSVReader(new FileReader(csvfile));  
		String [] nextLine;  
		int i=0;
	    int line =1;
		while ((nextLine = reader.readNext()) != null)  
		{  
		 if (i>-1) {
			 
			 System.out.println(line+" "+"AFA: "+nextLine[2] +" COUNTRY: "+nextLine[0]+
					 			" Email: "+nextLine[1] );
			 
			 Users u = new Users();
			 u.setAfA(nextLine[0]);
			 u.setEmail(nextLine[1]);
			 u.setCountry(nextLine[2]);
			 
			
			 lu.add(u);
			   
			}
		 		i=i+1;
		 		line=line+1;
		
		}
		
		}  
		catch (Exception e)   
		{  
		e.printStackTrace();  
		}  
		
		
		return lu;
	}
	
	
	public void writeToCsv(List<Users> userlist, String header) throws IOException {
		String path = System.getProperty("user.dir")+"/src/test/java/log/users.csv";
        PrintWriter writer = new PrintWriter(path);
        writer.println(header);

        for (Users sample : userlist) {
            writer.println(sample.toString());
        }
        writer.close();
    }

	
	
	
	    

}
