package com.test.ui;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import base.TestBase;
import objs.GooglePageFactory;

public class TestGF extends TestBase{
	
	
	GooglePageFactory gp;
	@Test(dataProvider = "test1")
	public void testGp(String text) {
		gp = new GooglePageFactory(driver);
		driver.get(getProp().getProperty("url"));
		gp.typeString(text).clickSerch().clickResult();
	}
	
	@DataProvider(name = "test1")
	public Object[][] createData1() {
	 return new Object[][] {
	   { "CA technologies"},
	   { "australia covid cases"},
	 };
	}

}
