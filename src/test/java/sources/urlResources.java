package sources;

public enum urlResources {
	
	//sit environemnt
	
	env("sit"),
	sit_salesforce("https://charteredaccountantsanz--sit.my.salesforce.com/"),
	sit_user("jack.xiong@charteredaccountantsanz.com.sit"),
	
	//qs environment
	qa_saleforce("https://charteredaccountantsanz--qa.my.salesforce.com/"),
	qa_user("jack.xiong@charteredaccountantsanz.com.qa ");
	
	private String path;
	
	urlResources(String path)
	{
		this.path = path;
	}
	public String getPath() {
		return this.path;
	}

}
