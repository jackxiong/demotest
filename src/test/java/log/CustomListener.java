package log;

import org.testng.TestListenerAdapter;
import base.TestBase;
import org.testng.ITestResult;




public class CustomListener extends TestListenerAdapter{
	
	private int m_count = 0;
	//WebDriver driver;
	 
	   @Override
	   public void onTestFailure(ITestResult tr) {
		  System.out.println("capture screen");
	      log(tr.getName()+ "--Test method failed from log\n");
	      TestBase.takeSnapShot(TestBase.getDriver(),"failed_"+tr.getName());
	   }
		 
	   @Override
	   public void onTestSkipped(ITestResult tr) {
	      log(tr.getName()+ "--Test method skipped from log\n");
	   }
		 
	   @Override
	   public void onTestSuccess(ITestResult tr) {
	      log(tr.getName()+ "--Test method success from log\n");
	      TestBase.takeSnapShot(TestBase.getDriver(),"passed_"+tr.getName());
	   }
		 
	   private void log(String string) {
	      System.out.print(string);
	      if (++m_count % 40 == 0) {
	         System.out.println("");
	      }
	   }

}
