package base;

import java.io.File;
import java.io.FileInputStream;
import java.text.DecimalFormat;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

public class TestBase {
	
	protected static WebDriver driver;

	static String driverPath = "drivers/"; //"./drivers/";
	
	@BeforeClass(alwaysRun=true)
	@Parameters({"browsers","OS"})
	public void setUp(String browsers,String OS) {

		System.out.println("start browser..."+browsers + driverPath+OS);
		
		if (browsers.equals("chrome")) {
			
				driver = chromeBrowser(OS);  //chromeBrowser(OS);
				
			}else if (browsers.equals("firefox")) {
				
				driver =firefoxBrowser();
				
			}else {
				
				driver =headlessBrowser(OS);
			}	
		 
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//String url = "https://www.google.com";
    	//driver.get(url);
		getDriver();
		

	}
	public WebDriver firefoxBrowser() {
		
		System.setProperty("webdriver.gecko.driver",driverPath+"geckodriver.exe"); 
		driver = new FirefoxDriver();
		
		return driver;
		
	}
	
	public WebDriver headlessBrowser(String os) {
		
		
		//chrome headless 
		if (os.equals("Linux")) {
			
			//driverPath = "/var/lib/jenkins/workspace/test/drivers/";
			System.out.println("=>"+driverPath+os+"/"+"chromedriver");
			System.setProperty("webdriver.chrome.driver",driverPath+os+"/"+"chromedriver"); 
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");
			options.addArguments("--headless"); 
			//options.addArguments("--headless", "--window-size=1920, 1080"); 
			options.setBinary("/usr/bin/google-chrome");
			driver = new ChromeDriver(options);
			
			
		}else {
			
			System.out.println("OS=>"+ os);
			System.setProperty("webdriver.chrome.driver",driverPath+os+"/"+"chromedriver");
			//System.setProperty("webdriver.chrome.driver", driverPath+os+"/"+"chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless"); 
			options.addArguments("--headless", "--window-size=1920, 1080"); 
			driver = new ChromeDriver(options);
			
		}
		
		
		return driver;
		
	}
	
	public WebDriver chromeBrowser(String os) {
		
		if (os.equals("Linux")) {
			
			// chrome instance browser
			//driverPath = "/var/lib/jenkins/workspace/test/drivers/";
			System.setProperty("webdriver.chrome.driver", driverPath+os+"/"+"chromedriver");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");
			options.setBinary("/usr/bin/google-chrome");
	        driver = new ChromeDriver(options);
			
		} else if  (os.equals("mac")) {
			
			 System.setProperty("webdriver.chrome.driver", driverPath+os+"/"+"chromedriver");
			 driver = new ChromeDriver();
		}
		else {
			
			System.setProperty("webdriver.chrome.driver", driverPath+os+"/"+"chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");
	        driver = new ChromeDriver(options);
		}
		
			return driver;
	}
	
	public static void takeSnapShot(WebDriver webdriver,String path) {
		 
		 String n =nums();
		 String fileWithPath = "./screens/"+path+"_"+n+".jpg";
		 
		 try {
			    TakesScreenshot scrShot =((TakesScreenshot)webdriver);
		        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
		        File DestFile=new File(fileWithPath);
		        FileUtils.copyFile(SrcFile, DestFile);
		        System.out.println("captured screen.."+fileWithPath);
		 } catch (Exception error) {
			 System.out.println("failed to capture screens");
			 error.printStackTrace();
		 }
	       

	   }
	
	 public static String nums() {
			
		    Random rand = new Random();
	        int num1 = (rand.nextInt(7) + 1) * 100 + (rand.nextInt(8) * 10) + rand.nextInt(8);
	        int num2 = rand.nextInt(743);
	        int num3 = rand.nextInt(10000);

	        DecimalFormat df3 = new DecimalFormat("000"); 
	        DecimalFormat df4 = new DecimalFormat("0000"); 

	        String phoneNumber = df3.format(num1) + "-" + df3.format(num2) + "-" + df4.format(num3);
	       
	        return phoneNumber;
	}
	 
	 public static WebDriver getDriver()

		{
			return driver;
		}
	 
	 public Properties getProp () {
			
			Properties prop = new Properties();;
			try {
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"/global.properties");
			System.out.println(System.getProperty("user.dir")+"/global.properties");
			prop.load(fis);
			}catch (Exception error) {
				error.printStackTrace();
			}
			
			return prop;
		}
	 

	@AfterClass(alwaysRun=true)
	public void tearDown() {
		
		driver.manage().deleteAllCookies();
		
		try {
			Thread.sleep(2*1000);
		}catch(Exception error) {
			error.printStackTrace();
		}
		driver.close();
		driver.quit();

	}

}
